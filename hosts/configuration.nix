#
# Global configuration for NixOS
#
#
# Referenced Files
# ================
#
# - flake.nix 
#    └─ ./hosts
#        ├─ default.nix <--- Variables derived from here
# -----> ├─ configuration.nix *
#        └─ ./<host>
#            └─ default.nix
#

{ config
, lib
, pkgs
, inputs
, username
, hostName
, directories
, ...
}:

let
  inherit (lib) mkDefault;
in {
  
  # Groups
  users.groups."sops" = {}; # Define existence

  # Users
  users.users.root.extraGroups = [ "sops" ];
  users.users."${username}" = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "sops" ];

    initialPassword = "password";
  };

  # Use up-to-date kernel
  #boot.kernelPackages = 

  # ---===[ Networking, Security, & SOPS ]===---#

  networking = { inherit hostName; };

  security = {
    rtkit.enable = true;
    polkit.enable = true;
  };

  # Enable GPG
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };
  services.pcscd.enable = true;

  # Enable SSH
  services.openssh.enable = true;
  #users.users.root.openssh.authorizedKeys.keyFiles = [
  #  (./. + "/${hostName}/ssh_host_ed25519_key.pub")
  #  (./. + "/${hostName}/ssh_host_rsa_key.pub")
  #];

  sops = {
    defaultSopsFile = ../secrets/secrets.yaml;
    defaultSopsFormat = "yaml";

    age.keyFile = "${directories.xdgConfigHome}/sops/age/keys.txt";

    secrets."fonts/comic-code-password" = {
      mode = "0440";
      group = "sops";
      restartUnits = [ "sops-fonts.service" ];
    };

  };

  # Run the following SOPS-related services
  # TODO: Re-encrypt comic-code-fonts.zip with something compatible with "unzip"
  systemd.services."sops-fonts" = let
    # Need absolute path
    comicCodeZip = "${directories.flake}/share/encrypted/comic-code-fonts.zip";
  in {
    script = ''
      mkdir -p /usr/share/fonts
      echo "command 1"
      echo ${comicCodeZip}
      unzip -v -P $(cat ${config.sops.secrets."fonts/comic-code-password".path}) ${comicCodeZip} -d /usr/share/fonts/
      echo "command 2"
      fc-cache
      echo "command 3"
    '';
    serviceConfig = {
      User  = "root";
      #Group = "sops";
      Type  = "simple";
    };

  };


  # ---===[ Fonts & Localisation ]===--- #

  time.timeZone = "America/Chicago";
  i18n.defaultLocale = "en_US.UTF-8";

  console = {
    font = "Comic Code";
    keyMap = "us";
  };


  # ---===[ System Packages & Aliases ]===--- #

  environment.systemPackages = with pkgs; [
    # --- CLI Applications ---
    age        # encryption tool for AGE keys
    bat        # prettified version of `cat`
    curl       # tool for transferring files w/ URL syntax
    dmidecode  # reads info about system's hardware from the BIOS
    eza        # more colorful version of `ls`
    git
    glxinfo     # test utilities for OpenGL
    gnupg       # GNU Privacy Guard - GPL OpenPGP implementation
    killall     # kill all instances of a certain program
    pandoc      # helps convert b/w different document formats
    parted      # CLI partitioning tool
    pbzip2      # parallelized implementation of `bzip2`
    pciutils    # programs for inspecting & manipulating PCI devices
    ripgrep
    sops
    ssh-to-age  # convert private ssh ed25519 keys to age keys
    ssh-to-pgp  # convert ssh keys to GPG keys
    thefuck     # corrects previous console command
    unzip
    wget
    wl-clipboard  # command-line copy/paste for Wayland
    youtube-dl
    zip
    zoxide      # Replacer for `cd` - learns your habits
        
    # --- TUI Applications ---
    btop  # a.k.a. bashtop
    htop
    powertop
  ];

  environment.shellAliases = {
    cat = "bat --plain";

    cd = "z";

    # grep
    
    # ls
    ls = "eza --icons=auto --color=auto";
    la = "eza --all --icons=auto --color=auto";
    ll = "eza --long --all --git --octal-permissions --icons=auto --color=auto";
    l  = "eza --classify --git --long --all --icons=auto --color=auto";
    lt = "eza --tree --long --all --icons=auto --color=auto";
    lh = "eza --sort=size --long -A --git --no-time --icons=auto --color=auto";
  };

  # Use in place of `.bashrc`
  programs.bash.interactiveShellInit = ''
    eval "$(zoxide init bash)"
  '';


  # ---===[ Global Hardware Configuration ]===--- #

  # Global ZRAM Configuration
  zramSwap = {
    enable = true;
    # Needs to be higher than disk swap priority (higher int is better)
    #   - in `swapDevices.*.priority`
    priority = mkDefault 100;
    # zstd - Fast w/ good compression
    #   - Need Linux kernel 4.14 or higher to enable
    algorithm = mkDefault "zstd";
    # Default memory percentage
    # - When storing Linux kernel 5.9 rc4, 
    #   - "lzo" has a compression ratio of 2.8:1
    #     - conservative 1.4:1 ratio -> ~70%
    #   - "zstd" has a compression ratio of 3.8:1
    #     - conservative ~2:1 ratio -> 100%
    memoryPercent = mkDefault ( 
      if (config.zramSwap.algorithm == "zstd") then 100 else 70
    );
  };


  # ---===[ Global Nix Settings ]===--- #

  nix = {
    # gets rid of duplicate files in "/nix/store"
    settings.auto-optimise-store = true;
    
    # Garbage Collection
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };
    	
    # Package to use throughout entire system
    package = pkgs.nixVersions.unstable;
    registry.nixpkgs.flake = inputs.nixpkgs;
    extraOptions = ''
      eval-cache            = false
      experimental-features = nix-command flakes
      keep-outputs          = true
      keep-derivations      = true
    '';
  };


  # DO NOT EDIT
  system.stateVersion = "23.11";


  # ---===[ Global Home Manager Settings ]===--- #

  home-manager.users."${username}" = {
    home = {
      inherit username;
      homeDirectory = directories.home;
    };

    fonts.fontconfig.enable = true;

    xdg = {
      enable = true;
      # Add Nix Packages to XDG_DATA_DIRS
      systemDirs.data = [ "${directories.home}/.nix-profile/share" ];
    };

    # Let Home Manager install and manage itself
    programs.home-manager.enable = true;

    # This value determines the Home Manager release that your configuration is
    # compatible with. This helps avoid breakage when a new Home Manager release
    # introduces backwards incompatible changes.
    #
    # You should not change this value, even if you update Home Manager. If you do
    # want to update the value, then make sure to first check the Home Manager
    # release notes.
    home.stateVersion = "23.11";  # DO NOT EDIT
  };
}
