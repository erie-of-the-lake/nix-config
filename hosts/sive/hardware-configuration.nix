{ config
, lib
, pkgs
, modulesPath
, ...
}:
let
  mkFsEntry = { subvol, disk ? /dev/disk/by-label/nixos }: {
    device = "${disk}";
    fsType = "btrfs";
    options = [
      "subvol=${subvol}"
      "compress=zstd"
      "commit=120"
      "discard=async"
      "noatime"
      "exec"
    ];
  };
in {
  imports =
    [ (modulesPath + "/profiles/qemu-guest.nix")
    ];

  boot.initrd.availableKernelModules = [ "ahci" "xhci_pci" "virtio_pci" "sr_mod" "virtio_blk" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = [ ];

  fileSystems = {
    "/"     = mkFsEntry { subvol = "@";     };
    "/home" = mkFsEntry { subvol = "@home"; };
  };

  boot.loader = {
    grub = {
      enable = true;
      enableCryptodisk = true;

      copyKernels   = true;
      efiInstallAsRemovable = true;
      efiSupport    = true;
      fsIdentifier  = "label";
      splashImage   = ../../share/wallpaper/deer-wallpaper.jpg;
      splashMode    = "fill";
      devices       = [ "nodev" ];
    };
    efi = {
      efiSysMountPoint = "/boot";
      canTouchEfiVariables = false;
    };
  };

  boot.initrd.luks.devices."cryptdata".device = "/dev/disk/by-label/crypted";

  fileSystems."/boot" = {
    device = "/dev/disk/by-label/boot";
    fsType = "vfat";
  };

  swapDevices = [{
    device = "/var/lib/swapfile";
    size = 2*1024;  # 2 GiB
  }];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.enp1s0.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
}