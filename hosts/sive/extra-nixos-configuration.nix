
{ pkgs, lib, ... }:

{
  imports = [
    ../../modules/desktops
  ];

  # From desktops import
  desktops.gnome.enable = true;
}
