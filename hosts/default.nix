#
# Main profiles for configuring Nix or NixOS per device.
# Setup will include Home Manager & NixOS or solely Home Manager.
#
# 
# Referenced Files
# ================
# 
# - flake.nix <--- Variables derived from here
#    └─ ./hosts
# -----> ├─ default.nix *
#        ├─ configuration.nix
#        └─ ./<host>
#            └─ default.nix
#
# TODO: Clean up and refactor code, esp. in let clause

# Flake Inputs
{ nixpkgs
, inputs
, home-manager
, flake-utils
, nur
# Added Variables
, username
, directories
, ...
}:

let
  # ---===[ Basic Variables ]===--- #

  # Used systems
  x86_64-linux = flake-utils.lib.system.x86_64-linux;
  aarch64-linux = flake-utils.lib.system.aarch64-linux;
	
  # Define `lib`
  # - NOTE: `pkgs` is defined in `setupSystem` b/c `system` system
  #   needs to be provided
  lib = nixpkgs.lib;


  # ---===[ Modules ]===--- #

  # Distro-agnostic Home Manager modules to import
  hmModules = {
    # Typically called for non-NixOS systems
    default = with inputs; [
      nixvim.homeManagerModules.nixvim
      nur.hmModules.nur
    ];
    # For systems with a Destkop Environment/Window Manager
    defaultDesktop = (
      import ../modules/browsers
      ++ import ../modules/editors
      ++ import ../modules/programs
    );
    # For systems acting as a server
    defaultServer = [ ];
  };
  # Modules for systems w/ NixOS + Home Manager
  nixosModules = {
    # Typically called for NixOS systems
    default = with inputs; [
      nixvim.nixosModules.nixvim
      nur.nixosModules.nur
      sops-nix.nixosModules.sops
    ];
    # Called for NixOS systems w/ a Desktop
  	defaultDesktop = (
      #import ../modules/desktops
      import ../modules/browsers
      ++ import ../modules/editors
      ++ import ../modules/programs
    );
  };
	
	
  # ---===[ Functions ]===--- #

  # Sets up a Nix or NixOS system given along with modules to be used in configuration.
  #
  # NOTE: Flake programs, e.g. `nixvim`, along with `inputs` will be inherited in
  #   `specialArgs` and in `extraSpecialArgs`.
  #
  # Parameters:
  #   - hostName: Name used to identify the system on a network
  #     - e.g. hostName = "nixos" -->."${username}"@nixos
  #   - host-path: Relative path (folder) belonging to the device/system
  #   	- e.g. "./raspberry-pi-4" or "./pudu"
  #       - defaults to "./${hostName}"
  #   - system: CPU system & Operating System of the system
  #     - e.g. x86_64-linux, aarch64-linux, aarch64-darwin, etc.
  #   - isNixos: Boolean value to declare if the underlying system's OS is NixOS
  #   - osModules: List of modules/imports + submodules used in `modules` of 
  #       `nixpkgs.lib.nixosSystem`
  #   - hmModules: List of modules/imports + submodules used in 
  #       `home-manager.users."${username}".imports` if `isNixos` is `true`.
  #       Otherwise declared in `modules` of `home-manager.lib.homeManagerConfiguration`
  #   - hmSharedModules: List of modules/imports + submodules used in 
  #       `home-manager.sharedModules` option, which are modules shared by all users
  #   - overlays: List of additional overlays/overlay submodules for nixpkgs
  #
  setupSystem = hostName: { 
    host-path ? ./. + "/${hostName}"
    , system
    , isNixos ? false
    # Added modules/overlays
    , osModules ? []
    , hmModules ? []
    , hmSharedModules ? []
    , overlays ? []
    , ...
  }:
    let
      inherit system;
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;   # Allow proprietary software
        overlays = [ nur.overlay ] ++ overlays;  # add extra overlays if applicable
      };
      # Attribute set of args to be inherited by `specialArgs` (for NixOS)
      # and `extraSpecialArgs` (for Home Manager) in both the if and else clauses.
      # - NOTE: `pkgs` needs to be inherited in `specialArgs`
      # - NOTE 2: `specialArgs` includes `modulesPath` by default
      commonSpecialArgs = {
        # Inputs & Flake programs + dependencies
        inherit inputs;
        # Variables instantiated in base `flake.nix`
        inherit username directories;
        # Parameters & Variables instantiated inside function scope
        inherit hostName isNixos;  # Function Parameters
        inherit system;            # Variables

        # Misc. arguments to be passed
        # ...
      };
      # Predefine both in case of a need to edit later
      specialArgs      = commonSpecialArgs // { inherit pkgs; };
      extraSpecialArgs = commonSpecialArgs;

      # Common Home Manager modules - imported regardless of OS
      # - NOTE: `hmModules` is optionally passed into function
      hmCommonModules = hmModules; 
    in
      if isNixos
      # Start NixOS configuration
      then ( lib.nixosSystem { 
        inherit system;
        inherit specialArgs;

        # `osModules` is optionally passed into function
        modules = osModules ++ [
          ./configuration.nix  # default, global configuration

          home-manager.nixosModules.home-manager {
            home-manager = {
              inherit extraSpecialArgs;
              # Packages
              useGlobalPkgs = true;
              useUserPackages = true;
              # Modules/Imports
              sharedModules = hmSharedModules;
              users."${username}".imports = hmCommonModules;
            };
          }
        ];
      })
      # Start standalone Home Manager configuration
      else ( home-manager.lib.homeManagerConfiguration {  
        inherit pkgs;
        inherit extraSpecialArgs;
            
        # NOTE: Can include basic config as a separate attribute set
        #   - e.g. `{ home = { inherit username; stateVersion = "23.11"; }; }`
        modules = hmCommonModules;
      });
	
in {
  # NOTE: All devices include the following files in their config:
  #   - "${host-path}"/default.nix
  #   - "${host-path}"/hardware-configuration.nix
  #
  # Additionally, all devices point to the following global config files:
  #   - ./configuration.nix

  # Desktop - AMD Ryzen 9 7950X3D + AMD RX 6800XT
  # - hostname: "taruca"
  #   - Named after hardiness of habitat, living at high altitudes in the Andes,
  #     either on rocky slopes or in the grasslands between mountains.
  taruca = setupSystem "taruca" {
    system = x86_64-linux;
    isNixos = true;

    osModules = [ ./taruca ]
      ++ nixosModules.default
      ++ nixosModules.defaultDesktop;
  };
	
  # Eluktronics RP-15 - AMD Ryzen 7 4800H + Nvidia RTX 2060 Mobile
  # - hostName: "rhim"
  #   - Rhim (or rhim gazelle) alludes to hardiness of mobility/agility of laptop
  #   - Lives in pockets throughout the Sahara such as Northern Algeria (into Tunisia
  #     and Libya) along isolated pockets in countries like Mali, Niger, Chad, Egypt,
  #     and Sudan.
  rhim = setupSystem "rhim" {
    system = x86_64-linux;
    isNixos = true;

    #hmModules = hmModules.defaultDesktop;
    osModules = [ ./rhim ] 
      ++ nixosModules.default
      ++ nixosModules.defaultDesktop;
      
    # NixGL needed on non-NixOS systems to resolve OpenGL issues
    # overlays = [ inputs.nixgl.overlay ]; 
  };

  # Raspberry Pi 4 - 2 GB RAM
  # - hostName: "pudu"
  #   - Meant to put emphasis on computer's small size
  #   - Consisting of two species, they are the "world's smallest deer." Both species
  #     live in the Northern and Southern Andes respectively.
  pudu = setupSystem "pudu" {
    system = aarch64-linux;
    isNixos = true;
        
    # hmModules = [];
    # shared-hmModules = [];
    osModules = nixosModules.default;
  };
	
  # NixOS VM
  # - hostName: "sive"
  #   - Sive's transformation into a deer, though transient, bears a loose resem-
  #     blance to the transient nature of virtual machines. Hence the hostName.
  #   - More common spellings are *Sadhbh* or *Sadb*, sometimes anglicized as *Sava*.
  #   - Chosen spelling originates from *Sive*, a play by John B. Keane about the
  #     character's life.
  # 
  ###  Irish Mythology (TODO make shorter)
  # 
  # NOTE: This paraphrased summary is based off the links below and basically acts as an excuse
  # for a creative writing exercise, something I haven't done in a while. Note that much of
  # Sive's story comes from retellings made throughout the 20th century.
  #  - https://www.ireland-information.com/irish-mythology/sadhbh-irish-legend.html
  #  - https://youtu.be/LD5X2_P0Qgo
  #  - https://en.wikipedia.org/wiki/Sadhbh
  #
  #     Sive was born the daughter of Bobd Derg, a king, in the Otherworld. Much like the stun-
  # ning lands surrounding her, Sive radiated a kind of beauty like none other, attracting the 
  # hearts of many an admirer, one including her mother's druid, Fear Doirche. Though uneased, 
  # Sive, timid and shy as she was, ignored the druid's gifts and advances for a time till her
  # discomfort became too disquieting to ignore. Gathering courage, Sive approached the druid, 
  # refusing his advances. Outraged, Fear Doirche cursed the woman to take the form of a deer and
  # chased after her. Nimble in her new form, Sive escaped the druid's clutches, seeking refuge 
  # in the human world. 
  #     For three years, Sive would escape hunters, both human and animal. One day, a large pack 
  # of hounds, no doubt led by many hunters, chased after her for leagues and leagues. Exhausted,
  # drained, two dogs had finally cornered her. Yet these dogs, the deer realized, were different
  # than the others. They were gentle, even playful. Once catching up, the dogs' owner, Fionn,
  # saw a uniqueness in this deer. He lent his arm out, careful not to startle the doe. This man,
  # so deliberate in his actions, Sive observed, bore a striking gentleness and kindess, even 
  # more so than the dogs. Curious yet still timid, Sive followed the stranger. Fionn, noticing
  # the deer, said little, allowing the animal residence and respite at his house upon arriving 
  # back. That next day, curled up on the ground, comfortable, Sive awoke to see not hooves but 
  # hands. The spell had broken. Reintroducing herself as Sive, she and Fionn became quick
  # friends, a romance soon sparking. Months passed, and love blossomed. 
  #    Yet life had no care for such a bond. A war against the Vikings had started, and Fionn was
  # called to lead an army. Still in the throes of her newfound love, Sive, so lovesick, checked 
  # every day for Fionn's return. One evening, her wish was granted: she saw Fionn again! Sive 
  # hurried outside, eager to embrace her love. But as soon as she drew near, Fionn's image 
  # vanished. No, instead, there stood Fear Doirche. It was an illusion, a trick. This whole
  # time, the druid had still been chasing after her. Aghast, Sive ran off, sprinting. Looking 
  # back, making distance from her stalker, Sive ran and ran and ran on her two legs, still 
  # running when those two legs became four. Fear Doirche may have been able to curse her again,
  # but he would never catch her. 
  #     After the end of the war, Fionn returned to an empty home. He searched and scoured all
  # over for Sive, his search lasting for days then months then years. And on his seventh year,
  # during one of his usual patrols in the woods, he found a peculiar boy. Staring past that 
  # dirty face and unkempt hair into those eyes, he realized he was staring at himself. Not
  # just himself but also Sive. This child was theirs. Naming the boy Oisín, Fionn would one day
  # raise the boy into a finer warrior than himself. However, there would never come a day where
  # he would behold his love's face again.
  #
  sive = setupSystem "sive" {
    system = x86_64-linux;
    isNixos = true;
        
    #hmModules = hmModules.defaultDesktop;
    osModules = [ ./sive ] ++ nixosModules.default;
      #	++ nixosModules.defaultDesktop;
  };
	
  # Linux VM - Non-NixOS Distribution
  # - hostName: "oisin"
  #   - `linux-vm` takes after `nixos-vm`, everything equal except the NixOS-related
  #     configurations. As a result, "oisin" is a fitting name, though the *í* needs
  #     to be replaced for an *i* to ensure ASCII compatibility.
  oisin = setupSystem "oisin" {
    system = x86_64-linux;
    isNixos = false;
          
    hmModules = hmModules.default;
  };
	
}
