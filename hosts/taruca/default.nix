
{ config
, pkgs
, lib
, ...
}:

let 
  inherit (lib) mkForce;
in {

  # ---===[ Desktop & GUI Programs ]===--- #

  ### Local Module Options
  # Enable GNOME & GDM
  desktops.gnome.enable = true;
  displayManager.gdm.enable = true;

  ### Global NixOS Options
  # Enable flatpak
  xdg.portal.enable = true;
  services.flatpak.enable = true;

  environment.systemPackages = with pkgs; [
    protonup-ng
    timeshift
    sleek-grub-theme
  ];

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
    gamescopeSession.enable = true;
  };

  # ---===[ Filesystem & Bootloader ]===--- #
  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/boot/EFI";
    };
    grub = {
      enableCryptodisk = true;
      efiSupport = true;
      configurationLimit = 50;
      device = nodev;
      theme = "${pkgs.sleek-grub-theme}";
    };
  };

  # VFIO-related config
  boot = {
    initrd.kernelModules = [
      "vfio_pci"
      "vfio"
      "vfio_iommu_type1"
    ];
    kernelParams = [ "amd_iommu=on" ];
  };

  hardware.opengl.enable = true;
  virtualisation = { 
    spiceUSBRedirection.enable = true;
  };
  programs.virt-manager.enable = true;

  specialisation = { 
    vfio.configuration = let
      # HACK: Remove once hardware & vfio modules are sufficient
      gpuIDs = [

      ];
    in {
      system.nixos.tags = [ "VFIO" ];

      boot.kernelParams = [("vfio-pci.ids=" + lib.concatStringStep "," gpuIDs)]
      
    };
  };
}
