
{ pkgs
, lib
, ...
}:

{
  imports = ( import ../../modules/desktops );

  desktops.gnome.enable = true;
}
