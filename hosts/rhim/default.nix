
{ config
, pkgs
, lib
, username
, ... 
}:

{
  imports = [ ./hardware-configuration.nix ] 
    ++ (import ../../modules/desktops);

  # ---===[ Desktop & GUI Programs ]===--- #

  ### Local Module Options
  # Enable GNOME
  # - TODO: Switch to ../../modules/desktops/gnome.nix options
  desktops.gnome.enable = true;
  displayManager.gdm.enable = true;

  ### Global NixOS Options
  # Enable Flatpak
  xdg.portal.enable = true;  # Necessary Prerequisite
  services.flatpak.enable = true;

  environment.systemPackages = with pkgs; [
    protonup-ng  # cli for Proton-GE
    timeshift  # System restore tool for Linux
    sleek-grub-theme  # sleek GRUB2 theme
  ];

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
    gamescopeSession.enable = true;
  };
  
  # ---===[ Filesystem & Bootloader ]===--- #
  fileSystems = {
    "/".options     = [ "compress=zstd" "ssd" ];
    "/home".options = [ "compress=zstd" "ssd" ];
    "/nix".options  = [ "compress=zstd" "ssd" "noatime" ];
  };

  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/boot";
    };
    grub = {
      enableCryptodisk = true;
      efiSupport = true;
      configurationLimit = 60;
      #efiInstallAsRemovable = true;  # in case canTouchEfiVariables doesn't work
      device = "nodev";
      theme = "${pkgs.sleek-grub-theme}";
      #splashImage = "/home/${username}/.nix/share/wallpaper/deer-wallpaper.jpg";
    };
  };
   
  specialisation = {

    nvidia.configuration = {
      system.nixos.tags = [ "with-nvidia" ];

      hardware.nvidia = {
        # Install Stable Nvidia Driver
        package = config.boot.kernelPackages.nvidiaPackages.stable;
        # Enable open-source kernel module
        open = true;

        modesetting.enable = true;  # Required
        
        # Experimental. Can cause sleep/suspend to fail
        powerManagement.enable = true;
        # Fine-grained power management. Turns off GPU when not in use
        # Can only be used on Turing Nvidia GPUs and newer
        #powerManagement.finegrained = true;

        # Enable Nvidia settings menu via `nvidia-settings`
        nvidiaSettings = true;
      };

      # Load Nvidia Driver for Xorg & Wayland
      services.xserver.videoDrivers = [ "nvidia" ];

      # Enable OpenGL
      hardware.opengl = {
        enable = true;
        driSupport = true;
        driSupport32Bit = true;
      };

    };


    vfio.configuration = let
      # 2060 Mobile
      gpuIDs = [
        "10de:1f15"  # VGA Compatible Controller
        "10de:10f9"  # HD Audio Controller
        "10de:1ada"  # USB 3.1 Host Contoller
        "10de:1adb"  # USB Type-C UCSI Contoller
      ];
    in {
      system.nixos.tags = [ "VFIO" ];
       
      # Enable VFIO Kernel Modules
      boot = {
        # NOTE: Need to manually add Nvidia kernel module to avoid build error
        extraModulePackages = [ 
          config.boot.kernelPackages.nvidia_x11_stable_open
        ];

        initrd.kernelModules = [
          "vfio_pci"
          "vfio"
          "vfio_iommu_type1"
          "vfio_virqfd"

          "nvidia"
          "nvidia_modeset"
          "nvidia_uvm"
          "nvidia_drm"
        ];

        kernelParams = [
          # Enable IOMMU
          "amd_iommu=on"
          # Add ID for ID in gpuIDs
          ("vfio-pci.ids=" + lib.concatStringsSep "," gpuIDs)
        ];

      };
      
      hardware.opengl.enable = true;
      virtualisation.spiceUSBRedirection.enable = true;
    };

  };
  
  /*
  boot.loader = {
    systemd-boot.enable = true;
    grub = { 
      enableCryptodisk = true;
      devices = [ "/dev/disk/by-label/boot" ];
    };
    efi = {
      efiSysMountPoint = "/boot";
      canTouchEfiVariables = true;
    };
  };
  */

}
