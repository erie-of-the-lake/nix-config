#
# Librewolf browser configuration for Home Manager
#

# Base Arguments
{ lib
, pkgs
, home-manager
# Added Variable Arguments
, ...
}:

{
    programs.librewolf = {
        enable = true;

        # Attribute set of Librewolf settings & overrides
        #   - For more info, see "https://librewolf.net/docs/settings/"
        # 
        # Example:
        # ```nix
        # { 
        #   "webgl.disabled" = false;
        #   "privacy.resistFingerprinting" = false;
        # }
        # ```
        #settings = {};  # Uncomment to adjust settings
    };
}