#
# Default browsers for Nix/NixOS installations with a Desktop Manager
# 
#
# Referenced Files
# ================
#
# - flake.nix 
#    ├─ ./hosts
#    │   └─ default.nix <--- Module called here
#    └─ ./modules
#        └─ ./browsers 
# ---------> ├─ default.nix *
#            └─ <browser>.nix 

[
    ./firefox.nix
    #./librewolf.nix
]
