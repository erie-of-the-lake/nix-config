#
# Firefox configurations in Home Manager
# 
# NOTE: The NUR flake is needs to be enabled
# 
# TODO:
#   - Import from `../icons`
#   - Refactor code, esp DuckDuckGo references
#
# NOTE: `user.js` has elements added in the following order:
#   - programs.firefox.profiles.<name>.bookmarks
#   - programs.firefox.profiles.<name>.extraConfig
#   - arkenfox's `user.js`
#   - `extraSettings`
#   - programs.firefox.profiles.<name>.bookmarks
#  
#
# Referenced Files
# ================
#
# - flake.nix 
#    └─ ./modules
#        ├─ ./browsers 
# -------│-> └─ firefox.nix *
#        └─ ./icons
#            └─ default.nix
#

# Base Arguments
{ config
, lib
, pkgs
, nur
# Added Variable Arguments
, username
, ...
}:

with lib;

let
  icons = import ../icons;

  nur-arkenfox-userjs = pkgs.nur.repos.ataraxiasjel.arkenfox-userjs;
  profile-name = "${username}";

  # TODO: Provide a means by which to edit DuckDuckGo JSON
  ddg-settings-json = ''
    {
      "kae": "d",
      "k1": "-1",
      "kaq": "-1",
      "k7": "282936",
      "kj": "3a3c4e",
      "k9": "62d6e8",
      "kaa": "b45bcf",
      "kx": "8a8cba",
      "k8": "e9e9f4",
      "k21": "3a3c4e",
      "kay": "b",
      "kau": "-1",
      "kap": "-1",
      "kao": "-1",
      "k18": "1",
      "kak": "-1",
      "kax": "-1"
    }
    '';
  ddg-homepage-url = "https://duckduckgo.com/?"
    + (pipe ddg-settings-json [
      # JSON String -> AttrSet
      builtins.fromJSON
      # AttrSet: { name = value; ... } -> List[String]: [ "${name}=${value}" ... ]
      (mapAttrsToList (name: value: "${name}=${value}"))
      # List[String]: [ "${name}=${value}" ... ] -> String, joined w/ sep as "&"
      (lst: concatStringsSep "&" lst)
  ]);


  # Attribute set of Firefox preferences. Casts `prefs` into 
  # `user_pref("${name}", ${value});`, something parseable by Firefox's `user.js`.
  # 
  # `extraSettings` is otherwise identical to 
  # `progrmas.firefox.policies.<name>.settings`, except that `prefs` gets
  # tacked onto the end of `user.js`, meaning that any declarations will override
  # anything declared from arkenfox's `user.js`.
  #
  # Parameters
  #   - prefs: Attribute set of Firefox preferences.
  #
  # Example:
  # ```nix
  # extraSettings {
  #   "browser.startup.homepage" = "https://nixos.org";
  #   "general.useragent.locale" = "en-GB";
  #   "browser.bookmarks.showMobileBookmarks" = true;
  #   "browser.newtabpage.pinned" = [{
  #       title = "NixOS";
  #       url = "https://nixos.org";
  #   }];
  # }
  # ```
  #
  extraSettings = prefs:
    # { "some.json.string" = [{ x = "foo"; y = "bar" }]; ... }
    # - name: "some.json.string"
    # - value: [{ x = "foo", y = "bar" }]
    pipe prefs [
      # JSONify the value except when value is bool, int, or string
      (mapAttrs (name: value: 
      builtins.toJSON (if isBool value || isInt value || isString value
        then value
        else builtins.toJSON value)
      ))
      # Map entry: { name = value; } -> ''user_pref("${name}", ${value});''
      (mapAttrsToList (name: value: ''user_pref("${name}", ${value});''))
      # Map List[String] -> String, w/ separator as "\n"
      (lst: concatStringsSep "\n" lst)
    ];
    
in {
  home-manager.users.${username} = {

    # ---===[ Dependencies ]===--- #

    # For icons when defining browser search engines
    #imports = [ ../icons ];
    # Needed for setting profile `user.js`
    home.packages = [ nur-arkenfox-userjs ];
    

    # ---===[ Basic Firefox Configuration ]===--- #
    programs.firefox.enable = true;

    
    # ---===[ Firefox Profiles ]===--- #
    programs.firefox.profiles.${profile-name} = {

      # ---===[ Basic Profile Configuration ]===--- #
      isDefault = true;


      # ---===[ Extensions/Addons ]===--- #
      extensions = with pkgs.nur.repos.rycee.firefox-addons; [
        bitwarden      # My default password manager - not self-hosted
        clearurls      # Removes tracking elements from URLs
        cookie-autodelete  # Auto-deletes cookies after tab closes
        decentraleyes  # Protects against "free", centralized CDNS
                       #   - e.g., Google Hosted Libraries
        sponsorblock   # Blocks YouTube sponsor segments
        ublock-origin  # Content- & Ad-Blocker
        xbrowsersync   # Anonymous bookmark sync
      ];


      # ---===[ Search Engines ]===--- #
            
      # See `SearchEngine.jsm` for current available options.
      # Available options for `programs.firefox.profiles.<name>.search.engines` include:
      #   - name: String (i.e. in `search.engines.<name>`)
      #   - description: String
      #   - icon: Path
      #   - iconUpdateURL: Url
      #   - updateInterval: Integer
      #   - definedAliases: List[String]
      #   - urls: List[AttributeSet] (see example)
      # 
      # `urls` example:
      # ```nix
      # # From Nix Packages search engine
      # urls = [{
      #    template = "https://search.nixos.org/packages";
      #    params = [
      #       { name = "type" ; value = "packages"; }
      #       { name = "query"; value = "{searchTerms}"; }
      #    ];
      # }]
      # ```
      search = {
        force = true;  # Force replaces existing search config
        default = "DuckDuckGo";
        order = [ "DuckDuckGo" "StartPage" ];  # order engines are listed in
      };
      search.engines = {
        # Built-ins
        "Amazon.com".metaData.hidden = true;
        "Bing".metaData.hidden = true;
        "Google".metaData.hidden = true;
        "eBay".metaData.hidden = true;
        "Wikipedia (en)".metaData.alias = "@w";

        # Extra Search Engines
        # - Blueprint: "https://gitlab.com/kira-bruneau/home-config/"
        #   - Reference file: "home-config/package/firefox/default.nix"
        "DuckDuckGo" = {
          urls = [{
            template = "https://duckduckgo.com/";
                    
            # See https://duckduckgo.com/duckduckgo-help-pages/settings/params/
            params = [{ name = "q"; value = "{searchTerms}"; }]
              ++ (pipe ddg-settings-json [
                # JSON String -> Attribute Set
                builtins.fromJSON
                # AttrSet: { name = value; } -> List[List[String]]: [ [ "${name}" "{value}" ] ... ]
                (mapAttrsToList (name: value: [ "${name}" "${value}" ] ))
                # List[List[String]]: [ [ "${name}" "{value}" ] ... ] -> AttrSet Name-Value Pairs
                (outer: forEach outer (  # for each inner list in outer list
                  inner: nameValuePair (builtins.elemAt inner 0) (builtins.elemAt inner 1)
                ))
              ]);
            definedAliases = [ "@d" "@ddg" ];
          }];
        };
        "GitHub" = {
          urls = [{
            template = "https://github.com/search";
            params = [{ name = "q"; value = "{searchTerms}"; }];
          }];
          icon = "${icons.logos.github}";
          definedAliases = [ "@gh" ];
        };
        "GitLab" = {
          urls = [{
            template = "https://gitlab.com/explore/projects";
            params = [
              { name = "name"; value = "{searchTerms}"; }
              { name = "sort"; value = "stars_desc"   ; }
            ];
          }];
          icon = "${icons.logos.gitlab}";
          definedAliases = [ "@gl" ];
        };
        "Home Manager Options" = {
          urls = [{
            template = "https://mipmip.github.io/home-manager-option-search";
            params = [{ name = "query"; value = "{searchTerms}"; }];
          }];
          icon = "${icons.logos.nix}";
          definedAliases = [ "@ho"  "@hmo" ];
        };
        "Nix Packages" = {
          urls = [{
            template = "https://search.nixos.org/packages";
            params = [
              { name = "channel"; value = "unstable"     ; }
              { name = "query"  ; value = "{searchTerms}"; }
            ];
          }];
          icon = "${icons.logos.nix}";
          definedAliases = [ "@np" ];
        };
        "NixOS Options" = {
          urls = [{
            template = "https://search.nixos.org/options";
            params = [
              { name = "channel"; value = "unstable"     ; }
              { name = "query"  ; value = "{searchTerms}"; }
            ];
          }];
          icon = "${icons.logos.nix}";
          definedAliases = [ "@no" ];
        };
        "NixOS Wiki" = {
          urls = [{
            template = "https://wiki.nixos.org/w/index.php";
            params = [{ name = "search"; value = "{searchTerms}"; }];
          }];
          icon = "${icons.logos.nix}";
          definedAliases = [ "@nw" ];
        };
        "StartPage" = {
          urls = [{
            template = "https://www.startpage.com/sp/search";
            params = [{ name = "query"; value = "{searchTerms}"; }];
          }];
          icon = "${icons.logos.startpage}";
          definedAliases = [ "@sp" ];
        };
        "YouTube" = {
          urls = [{
            template = "https://www.youtube.com/results";
            params = [{ name = "search_query"; value = "{searchTerms}"; }];
          }];
          icon = "${icons.logos.youtube}";
          definedAliases = [ "@y" "@yt" ];
        };
      };


      # ---===[ Containers ]===--- #
    
      # Available icons in `containers.<name>.icon`:
      #   - "briefcase", "cart" , "chill"      , "circle",
      #     "dollar"   , "fence", "fingerprint", "food"  ,
      #     "fruit"    , "gift" , "pet"        , "tree"  ,
      #     "vacation"
      #
      # Available colors in `containers.<name>.color`:
      #   - "blue"  , "turquoise", "green"  ,
      #     "yellow", "orange"   , "red"    ,
      #     "pink"  , "purple"   , "toolbar"
      #
      # `containers.<name>.id` increments by 10 for each profile just in case
      # I need to make a new profile in between extant ones.
      containers = {
        "Anonymous" = {
          id    = 10;
          icon  = "chill";
          color = "pink";
        };
        "Personal" = {
          id    = 20;
          icon  = "tree";
          color = "turquoise";
        };
        "Work" = {
          id    = 30;
          icon  = "briefcase";
          color = "orange";
        };
        "YouTube" = {
          id    = 40;
          icon  = "fruit";
          color = "red";
        };
        "Shopping" = {
          id    = 50;
          icon  = "cart";
          color = "yellow";
        };
        "Banking" = {
          id    = 60;
          icon  = "dollar";
          color = "green";
        };
        "Social Media" = {
          id    = 70;
          icon  = "pet";
          color = "blue";
        };
        # DEPRECATED: Near identical role as "Anonymous"
        #"Dangerous" = {
        #  id    = 70;
        #  icon  = "fingerprint";
        #  color = "red";
        #};
      };


      # ---===[ Extra Config ]===--- #
    
      # NOTE: Settings get written (and potentially overwritten) to `user.js`
      #   in the following order:
      #     - `profiles.<name>.settings`
      #     - `profiles.<name>.extraConfig`
      #     - `profiles.<name>.bookmarks`
      extraConfig = lib.fileContents "${nur-arkenfox-userjs}/share/user.js/user.js"
        + "\n" 
        + extraSettings {
          # Startup
          "browser.startup.page" = 1;  # 1 = Home
          "browser.startup.homepage" = "${ddg-homepage-url}";
          "browser.newtabpage.enabled" = true;
          # Delete 
          # Manually instantiate network referer header
          # - Reason: https://support.mozilla.org/en-US/questions/1163565
          "network.http.sendRefererHeader" = 1;
        };
    };
  };
}
