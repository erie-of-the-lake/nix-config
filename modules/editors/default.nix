#
# Default text editors (Desktop Environment not needed)
# 
# Referenced Files
# ================
# 
# - flake.nix 
#    ├─ ./hosts
#    │   └─ default.nix <--- Module called here
#    └─ ./modules
#        └─ ./editors 
# ---------> ├─ default.nix *
#            └─ <editor>.nix 
#

[
    ./neovim.nix
]