#
# Neovim Home Manager configurations
# 
# NOTE: nixvim is required
# 

# Base Arguments
{ lib
, pkgs
# Flake Programs + Added Flake Arguments
, ...
}:

{
  environment.variables = {
    EDITOR = "vim";
  };
  
  programs.nixvim = {
    enable        = true;
    enableMan     = true;  # Enables man pages via `man nixvim`
    viAlias       = true;
    vimAlias      = true;
    defaultEditor = true;
    
    colorschemes.catppuccin = {
      enable = true;
      transparentBackground = true;
      flavour = "mocha";
      /*
      colorOverrides.mocha = {
        rosewater = "#efc9c2";
        flamingo = "#ebb2b2";
        pink = "#f2a7de";
        mauve = "#b889f4";
        red = "#ea7183";
        maroon = "#ea838c";
        peach = "#f39967";
        yellow = "#eaca89";
        green = "#96d382";
        teal = "#78cec1";
        sky = "#91d7e3";
        sapphire = "#68bae0";
        blue = "#739df2";
        lavender = "#a0a8f6";
        text = "#b5c1f1";
        subtext1 = "#a6b0d8";
        subtext0 = "#959ec2";
        overlay2 = "#848cad";
        overlay1 = "#717997";
        overlay0 = "#63677f";
        surface2 = "#505469";
        surface1 = "#3e4255";
        surface0 = "#2c2f40";
        base = "#1a1c2a";
        mantle = "#141620";
        crust = "#0e0f16";
      };
      */
      

      #dimInactive.percentage = 0.15;

      integrations = {
        indent_blankline.enabled = true;
        neotree = true;
        rainbow_delimiters = true;
        treesitter = true;
      };
    };

    options = {
      syntax = "on";
      # Automatically wrap text extends screen length
      wrap = true;
      # Set internal & RPC communication - gives programmatic control of Nvim - encoding
      encoding     = "utf-8";
      fileencoding = "utf-8";
      # Show line numbers
      number         = true;
      relativenumber = true;
      # Allow mouse to change active line
      mouse = "a";

      # --- Search Patterns --- #
      hlsearch   = true;  # Highlight matching search patterns
      incsearch  = true;  # Enable incremental search
      ignorecase = true;  # nclude matching uppercase words w/ lowercase search term
      smartcase  = true;  # Include only uppercase words w/ uppercase search term

      # --- Indentation Settings --- #
      shiftwidth  = 4;
      autoindent  = true;
      smartindent = true;
      tabstop     = 4;
      softtabstop = 4;
      expandtab   = true;

      # --- Workspace Behavior --- #
      splitright = true;  # Open new `vsplit` workspaces to the right
      splitbelow = true;  # Open new `split` workspaces below
    };
    
    # Clipboard - Enable global copy/paste
    clipboard = {
      register = "unnamedplus";
      providers.wl-copy.enable = true;  # Use when running Wayland
    };
    
    plugins = {
      lightline = {  # Status line/tabline
        enable = true;
        #colorscheme = "rosepine_moon";  # Closest to catppuccin mocha
      };
      lastplace.enable = true;
      neo-tree = {
        enable = true;
        closeIfLastWindow     = true;  # Closes neo-tree if last window
        enableGitStatus       = true;  # Show git status
        enableModifiedMarkers = true;  # Show markers on unsaved files
      };
      nix.enable = true;
      nvim-autopairs.enable = true;  # Gives pairs to symbols like {}
      nvim-colorizer.enable = true;  # Puts color behind Hex values

      # --- Tree-sitter & Requisite Plugins --- #
      treesitter = {
        enable = true;
        indent  = true;  # Indents cursor at appropriate spot
        folding = false;  # Collapse blocks of code down
        nixGrammars = true;
        nixvimInjections = true;
        incrementalSelection.enable = true;
      };
      # Sets line for tabs
      indent-blankline = {
        enable = true;
        # See `:help ibl.config` for options
        scope = {
          enabled = true;
          highlight = [ "Function" "Label" ];
        };
      };
      # Rainbow brackets for certain languages
      rainbow-delimiters.enable = true;

      # --- Troublesome Plugins --- #
      #tagbar.enable = true;  # errors out install
    };
        
    extraPlugins = with pkgs.vimPlugins; [
      #nvim-completion-manager
      #nvim-cm-racer
      tagbar
    ];
        
    # ---=== KEYBINDINGS ===--- #
        
    keymaps = let
      # Maps set of `key`s to an `action` which will be executed, followed
      # by a `desc`, or description.
      # Returns an attribute set containing a nixified keymapping
      mapkeys = key: action: desc:
        { 
          inherit key action; 
          options = { inherit desc; };
        };
    in [
      # --- Basic Actions --- #
      # Save and write file
      (mapkeys "<C-s>" "<CMD>w<CR>" "Save current file")

      # --- Multi-Window Commands --- #
      ### Split workspace directions
      (mapkeys "<C-w><Left>"  "<C-w>h" "Split workspace left" )
      (mapkeys "<C-w><Down>"  "<C-w>j" "Split workspace down" )
      (mapkeys "<C-w><Up>"    "<C-w>k" "Split workspace up"   )
      (mapkeys "<C-w><Right>" "<C-w>l" "Split workspace right")
      ### Resizing workspaces - Ctrl+A = Adjust
      # Vim keybindings - Ctrl+Shift+w <key>
      (mapkeys "<C-a>h" "<C-w><" "Decrease workspace width" )
      (mapkeys "<C-a>j" "<C-w>+" "Increase workspace height")
      (mapkeys "<C-a>k" "<C-w>-" "Decrease workspace height")
      (mapkeys "<C-a>l" "<C-w>>" "Increase workspace width" )
      # Arrow keys - Ctrl+Shift+w <key>
      (mapkeys "<C-a><Left>"  "<C-w><" "Decrease workspace width" )
      (mapkeys "<C-a><Down>"  "<C-w>+" "Increase workspace height")
      (mapkeys "<C-a><Up>"    "<C-w>-" "Decrease workspace height")
      (mapkeys "<C-a><Right>" "<C-w>>" "Increase workspace width" )
      ### Execute actions 
      # Note: `options.splitbelow = true`
      (mapkeys "<C-w>t" "<CMD>split +term<CR>" "Open terminal below")

      # --- Neotree Keybindings --- #
      # Toggle NeoTree
      (mapkeys "<C-t>" "<CMD>Neotree toggle<CR>" "Toggle Neotree"     )
      # Instantiate NeoTree
      (mapkeys "<C-n>" "<CMD>Neotree<CR>"        "Instantiate Neotree")
    ];
        
  };

}
