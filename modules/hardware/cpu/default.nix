#
# Default modules for CPU hardware
#

{ config
, lib
, pkgs
, ...
}:

let
  inherit (lib) mkDefault mkEnableOption mkIf mkOption types;

  cfg = config.hardware.cpu;
  # Global CPU config
  #frequencySpecsCfg = config.hardware.cpu.frequencySpecs;
  powerCfg = config.hardware.cpu.powerManagement;
  # AMD config
  amdCfg = config.hardware.cpu.amd;
in {
  options = let
    # Makes options for `scaling_min_freq` and `scaling_max_freq` for the CPU's
    # frequency scaler. 
    #
    # Parameters:
    # -----------
    #   minFreqDefault: nullOr int
    #     Default minimum frequency (in kHz) the CPU will scale to.
    #     If left null, a minimum set frequency will not be defined. 
    #   maxFreqDefault: nullOr int
    #     Default maximum frequency (in kHz) the CPU will scale to.
    #     If left null, a maximum set frequency will not be defined.
    #   descriptionAddendum: string
    #     What string to add to the end of the default description for each scaling
    #     frequency option.
    #     The default string is, "The minimum/maximum frequency (in kHz) the CPU will 
    #     scale to"
    #     
    mkScalingFreqOptions = { 
      minFreqDefault ? null
      , maxFreqDefault ? null 
      , descriptionAddendum ? ""
    }:
      {
        scaling_min_freq = mkOption {
          type = types.nullOr types.int;
          default = minFreqDefault;
          example = 2500000;
          description = lib.concatStrings [
            "The minimum frequency (in kHz) the CPU will scale to "
            descriptionAddendum
          ];
        };
        scaling_max_freq = mkOption {
          type = types.nullOr types.int;
          default = maxFreqDefault;
          example = 4800000;
          description = lib.concatStrings [
            "The maximum frequency (in kHz) the CPU will scale to "
            descriptionAddendum
          ];
        };
      };

  in {
    hardware.cpu = {
      # --- Global Power Management Profiles --- #
      /*
      frequencySpecs = {
        minimum = mkOption {
          type = types.nullOr types.int;
          default = null;
          example = 800000;
          description = ''
            The absolute minimum frequency in kHz the CPU is designed to use.
          '';
        };
        base = mkOption {
          type = types.nullOr types.int;
          default = null;
          example = 2900000;
          description = ''
            The base clock speed (or BCLK) in kHz the CPU is designed to run at.
          '';
        };
        max = mkOption {
          type = types.nullOr types.int;
          default = null;
          example = 4800000;
          description = "The maximum frequency (in kHz) the CPU is designed to use.";
        };
      }; 
      */

      powerManagement = {
        enable = mkEnable "management of the laptop's performance profile";

        behaviorType = mkOption {
          type = with types; 
            nullOr enum [ "powersave" "conservative" "default" "performance" ];
          default = null;
          example = "powersave";
          description = ''
            If the option is not null, the CPU's behavior will act according to the
            enum option specified. For instance, if `powersave` is selected, then the
            CPU will try to use as little power as possible. The available behaviors 
            are as follows:
            - *powersave:* If possible, the CPU governor will be set to `powersave`,
              and other optimisations will be used such as disabling frequency turbo if
              the system is discharging a battery.
            - *conservative:* If possible, the CPU governor will be set to 
              `conservative`, and other optimisations such as limiting - importantly 
              not disabling - frequency turbo so as to present some performance while
              still focusing primarily on power-saving.
            - *default:* The CPU governor will be set to `ondemand`, and no other
              optimisation(s) will be used to increase performance or reduce power.
            - *performance:* If possible, the CPU governor will be set to
              `performance`, and other optimisations will be used to abet the maximum
              CPU performance possible.
          '';
        };
        
        battery = mkScalingFreqOptions {
          descriptionAddendum = "while the system is using its battery.";
        };
        charger = mkScalingFreqOptions {
          descriptionAddendum = "while the system is using AC power.";
        };
      };

    # --- AMD-Specific Options --- #
      amd = {
        enablePstate = mkEnableOption ''
          the `amd-pstate` performance scaling driver. No cange will be made if the
          kernel version is older than 6.3. (Note that enabling `amd-pstate` is only
          available on Zen 2 and newer processors.)

          More documentation can be found at
          https://www.kernel.org/doc/html/latest/admin-guide/pm/amd-pstate.html
        '';
      };
  };


  config = mkIf powerCfg.enable {

    powerManagement.governor = "${powerCfg.behaviorType}";
    services.auto-cpufreq = let
      # Make if `option` is an integer
      mkIfOptionIsInt = option: mkIf (builtins.isInt option) option;
    in {
      enable = true;
      # MAYBE: Make `scaling_min_freq` and `scaling_max_freq` adjustable based on 
      # `hardware.cpu.frequencySpecs.<frequency>`
      battery = {
        governor = "${powerCfg.behaviorType}";
        turbo = 
          if (powerCfg.behaviorType != "powersave") then "auto"
          else "never";
        scaling_min_freq = mkIfOptionIsInt powerCfg.battery.scaling_min_freq;
        scaling_max_freq = mkIfOptionIsInt powerCfg.battery.scaling_max_freq;
      };
      charger = {
        governor = "${powerCfg.behaviorType}";
        turbo = "auto";
        scaling_min_freq = mkIfOptionIsInt powerCfg.charger.scaling_min_freq;
        scaling_max_freq = mkIfOptionIsInt powerCfg.charger.scaling_max_freq;
      };
    };

  } // mkIf amdCfg.enablePstate {

    hardware.cpu.amd.updateMicrocode = lib.mkDefault true;
    boot = mkIf (amdCfg.enablePstate
      && lib.versionAtLeast config.boot.kernelPackages.kernel.version "6.3") {
      
      kernelModules = [ "amd-pstate" ];
      kernelParams  = [ "amd_pstate=active" ];
    };

  };
};
