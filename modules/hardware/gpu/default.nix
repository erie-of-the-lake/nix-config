#
# Options for Intel, AMD, and Nvidia GPUs
#

{ config
, lib
, ...
}:

let
  inherit (lib) mkOption types;

  # Taken from nixpkgs/nixos/hardware/video/nvidia.nix in github:NixOS/nixpkgs
  # Type for PCI Bus IDs (e.g., "PCI:0:18:6")
  busIDType = types.strMatching "([[:print:]]+[\:\@][0-9]{1,3}\:[0-9]{1,2}\:[0-9])?";
  # Type for PCI vendor and device IDs (e.g., "10de:1f15")
  deviceIDType = types.strMatching "([[:xdigit:]]{4}\:[[:xdigit:]]{4})?";
in {

  options.hardware.gpu = let

    # Transform `busId` of type `busIDType` to a PCI location.
    #   e.g., "PCI:12:0:1" -> "12:00.1"
    busIdToLocation = busId:
      with lib; pipe "${busId}" [
        (s: splitString ":" s)                 # "PCI:6:18:0" -> [ "PCI" "6" "18" "0" ]
        (lst: drop 1 lst)                      #              -> [ "6" "18" "0" ]
        (lst: zipListsWith                 
          (a: b: if stringLength b == 1 then a+b else b)
          [ "0" "0" "" ] lst )                 #              -> [ "06" "18" "0" ]
        (lst: zipListsWith (a: b: a+b) lst [":" "." ""])  #   -> [ "06:" "18." "0" ]
        concatStrings                          #              -> "06:18.0"
      ];

    # Make an option documenting the PCI Bus ID of a target GPU.
    #
    # Parameters:
    # -----------
    #   targetGpuName: string
    #     The name of the class of GPU (e.g. "AMD iGPU"). This name will show up in
    #     the option's `description`. 
    #   exampleBusId: busIDType (special type of string)
    #     An example bus ID for the GPU. This example will show up in the option's
    #     `example` and `description`.
    mkBusIdOption = { targetGpuName, exampleBusId }:
      mkOption {
        type = busIDType;
        default = "";
        example = "${exampleBusId}";
        description = ''
          Bus ID of the ${targetGpuName}. You can find this ID using `lspci`.
          For example, if `lspci` shows the ${targetGpuName} at 
          "${busIdToLocation exampleBusId}", then you would set this option 
          to "${exampleBusId}".
        '';
      };

    # Make an option documenting the PCI Device ID(s) of a target GPU.
    mkDeviceIdOption = targetGpuName: 
      mkOption {
        type = types.listOf deviceIDType;
        default = [];
        example = [ "10de:1f15" "10de:10f9" ];
        description = ''
          Device ID(s) of the ${targetGpuName}. You can find its ID(s) using 
          `lspci -n` or `lspci -nn`, the latter showing both PCI device and vendor
          codes (i.e. which combines to make the device ID) as well as names.
        '';
      };

    # Creates PCI Bus ID and Device ID options in the same attribute set by calling
    # `mkBusIdOption` and `mkDeviceIdOption` respectively.
    mkBusAndDeviceIdOptions = { targetGpuName, exampleBusId }:
      { busId = mkBusIdOption { inherit targetGpuName exampleBusId };
        deviceId = mkDeviceIdOption targetGpuName;
      };
  in {
    # --- AMD Settings --- #
    amd = let exampleBusId = "PCI:4:0:0"; in {
      # Bus IDs & device IDs
      dgpu = mkBusAndDeviceIdOptions {
        targetGpuName = "dedicated AMD GPU";
        inherit exampleBusId;
      };
      igpu = mkBusAndDeviceIdOptions {
        targetGpuName = "AMD APU";
        inherit exampleBusId;
      };
    };

    # --- Intel Settings --- #
    intel = let exampleBusId = "PCI:0:2:0"; in {
      dgpu = mkBusAndDeviceIdOptions {
        targetGpuName = "dedicated Intel GPU";
        inherit exampleBusId;
      };
      igpu = mkBusAndDeviceIdOptions {
        targetGpuName = "Intel iGPU";
        inherit exampleBusId;
      };
    };

    # --- Nvidia Settings --- #
    nvidia = mkBusAndDeviceIdOptions {
      targetGpuName = "dedicated Nvidia GPU";
      exampleBusId  = "PCI:1:0:0";
    };
  };
}
