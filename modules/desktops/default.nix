#
#  Configuration for Desktop Environments & Window Managers
#
# Referenced Files
# ================
#
#  - flake.nix
#     ├─ ./hosts
#     │   └─ default.nix
#     └─ ./modules
#         └─ ./desktops
# ----------> ├─ default.nix *
#             └─ <desktop>.nix
#

[
  #./cosmic.nix
  ./gnome.nix
  #./sway
]