#
# SwayFX Options
#

{ config
, lib
, pkgs
, ...
}:

let 
    inherit (lib) mkEnableOption mkIf mkOption types;

    cfg = config.wayland.windowManager.sway.fx;
in {
    # TODO: May have to change many options to `types.nullOr types.<otherType>`
    options = {
        wayland.windowManager.sway.fx = {
            enable = {
                type = types.bool;
                default = false;
                description = ''
                    Whether to enable SwayFX. Enabling this option will override
                    {var}`wayland.windowManager.sway.package`.

                    Options specific to only SwayFX are located in
                    {var}`wayland.windowManager.sway.fx.config`
                '';
            };

            config = {

                # --- Blur --- #
                blur = {
                    enable = mkEnableOption "blur on windows";

                    xray.enable = mkOption {
                        type = types.bool;
                        default = false;
                        description = ''
                            Enabling this setting will set floating windows to blur based on the
                            background and not the windows below.
                        '';
                    };
                    passes = mkOption {
                        type = types.int.between 0 10;
                        default = 2;
                        description = "Number of passes the blur takes";
                    };
                    radius = mkOption {
                        type = types.int.between 0 10;
                        default = 5;
                    };
                    noise = mkOption {
                        type = types.float.between 0.0 1.0;
                        default = 0.0;
                        example = 0.1;
                        description = "Percentage of noise to add to windows";
                    };
                    brightness = mkOption {
                        type = types.float.between 0.0 2.0;
                        default = 1.0;
                        example = 1.25;
                        description = "Percentage of original brightness to adjust";
                    };
                    contrast = mkOption {
                        type = types.float.between 0.0 2.0;
                        default = 1.0;
                        example = 1.25;
                        description = "Percentage of original contrast to adjust";
                    };
                    saturation = mkOption {
                        type = types.float.between 0.0 2.0;
                        default = 1.0;
                        example = 1.25;
                        description = "Percentage of original saturation to adjust";
                    };
                };

                # --- Corners --- #
                corner.radius = mkOption {
                    type = types.int.unsigned;
                    default = 0;
                    example = 5;
                    description = "Radius (in pixels) for window corners";
                };
                
                # --- Shadows --- #
                shadows = {
                    enable = mkEnableOption "shadows for windows";

                    csd = mkEnableOption ''
                        shadows for Client-Side Decoration (e.g. GTK header bars). 

                        Note that the shadow might not fit on some windows.
                    '';
                    blurRadius = mkOption {
                        type = types.int.between 0 100;
                        default = 20;
                        description = "Radius (in pixels) for shadows on windows";
                    };
                    color = mkOption {
                        type = types.str;
                        default = "#000000";
                        example = "#3D3D3D";
                        description = "Hex value of the shadow color";
                    };
                    alpha = mkOption {
                        type = types.ints.u8;
                        default = 127;
                        description = ''
                            Integer value for alpha transparency, 0 being fully transparent and 255 
                            being fully opaque.
                        '';
                    };
                };

                # --- LayerShell Effects --- #
                layerShell = mkOption {
                    default = {};
                    description = ''
                        Attribute set of namespaces for which to apply LayerShell effects
                        (e.g. panels, notifications, etc.).

                        Current layer namespaces can be shown with
                        ```console
                        $ swaymsg -r -t get_outputs | jq '.[0].layer_shell_surfaces | .[] | .namespace'
                        ```
                    '';
                    # tranlates to `<...>.layerShell.<namespace>.<options>`
                    type = types.attrsOf (types.submodule ({namespace, ...}: {
                        options = {
                            blur.enable = mkEnableOption "blur on the specified namespace";
                            blur.ignoreTransparent = mkOption {
                                type = types.bool;
                                default = false;
                                description = ''
                                    Whether to ignore transparencies on the specified namespace
                                '';
                            };
                            shadows = mkEnableOption "shadows behind the specified namespace";
                            corner.radius = mkOption {
                                type = types.int.between 0 100;
                                default = 0;
                                description = ''
                                    Radius (in pixels) of the corners of the specified namespace
                                '';
                            };
                        };
                    }));
                };

                # --- Dimming --- #
                # TODO: Look at Home Manager documentation for
                #   `wayland.windowManager.sway.config`
                #   and bring options and option types in line with aid options.
                #
                dimInactive = {
                    default = mkOption {
                        type = types.float.between 0.0 1.0;
                        default = 0.0;
                        description = ''
                            Default value by which to dim all windows (`0.0` indicates no dimming
                            whereas `1.0` indicates full dimming).
                        '';
                    };
                    colors.unfocused = {
                        color = mkOption {
                            type = types.str;
                            default = "#000000";
                            example = "#3D3D3D";
                            description = "Hex value of the shadow color";
                        };
                        alpha = mkOption {
                            type = types.ints.u8;
                            default = 127;
                            description = ''
                                Integer value for alpha transparency, 0 being fully transparent and 255 
                                being fully opaque.
                            '';
                        };
                    };
                    colors.urgent = {
                        color = mkOption {
                            type = types.str;
                            default = "#000000";
                            example = "#3D3D3D";
                            description = "Hex value of the shadow color";
                        };
                        alpha = mkOption {
                            type = types.ints.u8;
                            default = 127;
                            description = ''
                                Integer value for alpha transparency, 0 being fully transparent and 255 
                                being fully opaque.
                            '';
                        };
                    };
                };

                # --- Misc. --- #
                # TODO: 
                #   - Application saturation
                #   - Titlebar separator
                #   - Scratchpad minimize
            };
        };
    };


}