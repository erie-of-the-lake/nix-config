#
# Sway Configuration
# Enable with "sway.enable = true;"
#  
# Templated off of:
# https://github.com/MatthiasBenaets/nixos-config/blob/master/modules/desktops/sway.nix
#

# Base Arguments
{ config
, lib
, pkgs
# Added Variable Arguments
, username
, hostName
, directories
, ...
}:

let
  inherit (lib) mkEnableOption mkIf toHexString;

  # TODO: Document function
  decimalToRgba = r: g: b: a:
    "#" + toHexString r
      + toHexString g 
      + toHexString b
      + toHexString a;
in {
  programs.sway = {
    enable = true;
    package = "${pkgs.swayfx}";
    extraPackages = with pkgs; [
      autotiling  # Autotiling plugin for Sway
    ];
  };

  home-manager.users."${username}" = {
    windows.windowManager.sway = {
      enable = true;
      systemd.enable = true;

      config = let 
        super = "Mod4";
        terminal = "${pkgs.kitty}/bin/kitty";   # TODO: don't make hardcoded
        menuProgram = "${pkgs.wofi}/bin/wofi";  # TODO: same here
        menu = "${menuProgram} -show drun";
        screenshot = "${pkgs.flameshot}/bin/flameshot";
      in {
        inherit terminal;
        inherit menu;
        modifier = "${super}";

        # Startup
        startup = [
          { command = "${pkgs.autotiling}/bin/autotiling"; always = true; }
        ];

        gaps = {
          inner = 5;
          outer = 5;
        };

        input = {
          "type:touchpad" = {
            tap = "enabled";
            dwt = "enabled";  # Disable while typing
            middle_emulation = "enabled";
            scroll_method = "two_finger";
            # method    | lrm          | lmr          |
            # ----------+--------------+--------------+
            # 1 finger  | left click   | left click   |
            # 2 fingers | right click  | middle click |
            # 3 fingers | middle click | right click  |
            tap_buttom_method = "lrm";
          };
          "type:keyboard" = {
            xkb_layout  = "us";
            xkb_numlock = "enabled";
          };
        };

        output = if (hostName == "rhim") then {
          "*".bg = "${directories.flake}/share/wallpaper/deer-wallpaper.jpg fill";
          "*".scale = "1";
          "eDP" = { mode = "1920x1080"; pos = "0 0"; };
        } else {};

        workspaceOutputAssign = if (hostName == "rhim") then [
            {output = "eDP"; workspace = "1";}
            {output = "eDP"; workspace = "2";}
            {output = "eDP"; workspace = "3";}
            {output = "eDP"; workspace = "4";}
            {output = "eDP"; workspace = "5";}
          ] else [];
        defaultWorkspace = "workspace number 1";
        /*
            "${modifier}+Escape" = "exec swaymsg exit";     # Exit
            "${modifier}+Return" = "exec ${terminal}";      # Terminal
            "${modifier}+space" = "exec ${menu}";           # Menu
            "${modifier}+e" = "exec ${pkgs.pcmanfm}/bin/pcmanfm"; # File Manager
            "${modifier}+l" = "exec ${pkgs.swaylock-fancy}/bin/swaylock-fancy"; # Lock Screen

            "${modifier}+r" = "reload";                     # Reload Environment
            "${modifier}+q" = "kill";                       # Kill
            "${modifier}+f" = "fullscreen toggle";          # Fullscreen
            "${modifier}+h" = "floating toggle";            # Floating

            "${modifier}+Left" = "focus left";              # Focus
            "${modifier}+Right" = "focus right";
            "${modifier}+Up" = "focus up";
            "${modifier}+Down" = "focus down";

            "${modifier}+Shift+Left" = "move left";         # Move
            "${modifier}+Shift+Right" = "move right";
            "${modifier}+Shift+Up" = "move up";
            "${modifier}+Shift+Down" = "move down";

            "Alt+Left" = "workspace prev_on_output";        # Navigate workspaces
            "Alt+Right" = "workspace next_on_output";

            "Alt+1" = "workspace number 1";                 # Open Workspace
            "Alt+2" = "workspace number 2";
            "Alt+3" = "workspace number 3";

            "Alt+Shift+Left" = "move container to workspace prev, workspace prev";    # Move to Workspace
            "Alt+Shift+Right" = "move container to workspace next, workspace next";

            "Alt+Shift+1" = "move container to workspace number 1";     # Move to Workspace
            "Alt+Shift+2" = "move container to workspace number 2";
            "Alt+Shift+3" = "move container to workspace number 3";
            "Alt+Shift+4" = "move container to workspace number 4";
            "Alt+Shift+5" = "move container to workspace number 5";

            "Control+Up" = "resize shrink height 20px";     # Resize
            "Control+Down" = "resize grow height 20px";
            "Control+Left" = "resize shrink width 20px";
            "Control+Right" = "resize grow width 20px";

            "Print" = "exec ${pkgs.flameshot}/bin/flameshot gui"; # Screenshots

            "XF86AudioLowerVolume" = "exec ${pkgs.pamixer}/bin/pamixer -d 10";   # Volume
            "XF86AudioRaiseVolume" = "exec ${pkgs.pamixer}/bin/pamixer -i 10";
            "XF86AudioMute" = "exec ${pkgs.pamixer}/bin/pamixer -t";             # Media
            "XF86AudioMicMute" = "exec ${pkgs.pamixer}/bin/pamixer --default-source -t";

            "XF86MonBrightnessDown" = "exec ${pkgs.light}/bin/light -U  5";      # Brightness
            "XF86MonBrightnessUp" = "exec ${pkgs.light}/bin/light -A 5";
        */
        keybindings = 
          let resizeAmt = 20; in{ 
          # Program Shortcuts
          "${super}"   = "exec ${menu}";
          "${super}+t" = "exec ${terminal}";
          "Print"      = "exec ${screenshot} gui";  # Screenshots
          
          # Shortcuts to programs
          "${super}+r" = "reload";           # Reload Environment
          "${super}+q" = "kill";             # Kill
          #"${super}+f" = "fullscreen toggle";          # Fullscreen
          "${super}+g" = "floating toggle";  # Floating

          ### Manipulating Windows
          # Focus
          "${super}+Left"  = "focus left";    
          "${super}+Right" = "focus right";
          "${super}+Up"    = "focus up";
          "${super}+Down"  = "focus down";
          # Move to different workspace
          "${super}+Shift+Left"  = "move container to workspace prev, workspace prev";    # Move to Workspace
          "${super}+Shift+Right" = "move container to workspace next, workspace next";
        };

        extraConfig = 
          let 
            allWindows = ''[app_id=".*"]'';
            resizeAmt  = "20px";  # num pixels
            # Colors
            #shadowRgba = decimalToHex r g b a;
            #dimUnfocusedRgba = decimalToHex r g b a;
            #dimInactiveRgba  = decimalToHex r g b a;
          in ''
          # ---===[ Keybinding Modes ]===--- #
          set $mode_move_and_resize_window "Move and resize window"
          mode $mode_move_and_resize_window {
              # Move
              bindsym Left  ${allWindows} move left
              bindsym Right ${allWindows} move right
              bindsym Up    ${allWindows} move up
              bindsym Down  ${allWindows} move down
              # Resize
              bindsym Shift+Left  ${allWindows} resize shrink width ${resizeAmt}
              bindsym Shift+Right ${allWindows} resize grow width ${resizeAmt}
              bindsym Shift+Up    ${allWindows} resize shrink height ${resizeAmt}
              bindsym Shift+Down  ${allWindows} resize grow height ${resizeAmt}
              # Return to "default" mode
              bindsym Return mode "default"
              bindsym Escape mode "default"
          }
          binsym $mod+Return mode $mode_move_and_resize_window

          # ---===[ SwayFX Options ]===--- #

          # Window background blur
          blur on
          blur_xray off
          blur_passes 2
          blur_radius 5

          # Window shadows
          shadows on
          shadows_on_csd off
          shadow_blur_radius 20
          shadow_color #0000007F
        '';
      };
    };
  };
}
/*
  config = mkIf (config.sway.enable) {
    wlwm.enable = true;                     # Wayland Window Manager

    environment = {
      loginShellInit = ''
        if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
          exec sway
        fi
      '';                                   # Start from TTY1
      variables = {
        #LIBCL_ALWAYS_SOFTWARE = "1";       # Needed for VM
        #WLR_NO_HARDWARE_CURSORS = "1";
      };
    };

    programs = {
      sway = {                              # Window Manager
        enable = true;
        package = [ pkgs.swayfx ];
        extraPackages = with pkgs; [
          autotiling      # Tiling Script
          wev             # Event Viewer
          wl-clipboard    # Clipboard
          wlr-randr       # Monitor settings
          xwayland        # X for Wayland
        ];
      };
    };

    home-manager.users.${username} = {
      wayland.windowManager.sway = {
        enable = true;
        systemd.enable = true;
        config = rec {
          modifier = "Mod4";
          terminal = "${pkgs.kitty}/bin/kitty";
          menu = "${pkgs.wofi}/bin/wofi -show drun";

          startup = [
            {command = "${pkgs.autotiling}/bin/autotiling"; always = true;}
          ];

          bars = [];                        # Using Waybar

          fonts = {
            names = [ "Source Code Pro" ];
            size = 10.0;
          };

          gaps = {
            inner = 5;
            outer = 5;
          };

          input = {                         # Input modules: $ man sway-input
            "type:touchpad" = {
              tap = "enable ";
              dwt = "enabled";
              scroll_method = "two_finger";
              middle_emulation = "enabled";
              natural_scroll = "enabled";
            };
            "type:keyboard" = {
              xkb_layout = "us";
              xkb_numlock = "enabled";
            };
          };

          output = if (hostName == "rhim") then {
            "*".bg = "~/.config/wall fill";
            "*".scale = "1";
            "eDP" = {
              mode = "1920x1080";
              pos = "0 0";
            };
          } else {};

          workspaceOutputAssign = if (hostName == "rhim") then [
            {output = "eDP"; workspace = "1";}
            {output = "eDP"; workspace = "2";}
            {output = "eDP"; workspace = "3";}
            {output = "eDP"; workspace = "4";}
            {output = "eDP"; workspace = "5";}
          ] else [];
          defaultWorkspace = "workspace number 1";

          colors.focused = {
            background = "#999999";
            border = "#999999";
            childBorder = "#999999";
            indicator = "#212121";
            text = "#999999";
          };

          keybindings = {                                   # Hotkeys
            "${modifier}+Escape" = "exec swaymsg exit";     # Exit
            "${modifier}+Return" = "exec ${terminal}";      # Terminal
            "${modifier}+space" = "exec ${menu}";           # Menu
            "${modifier}+e" = "exec ${pkgs.pcmanfm}/bin/pcmanfm"; # File Manager
            "${modifier}+l" = "exec ${pkgs.swaylock-fancy}/bin/swaylock-fancy"; # Lock Screen

            "${modifier}+r" = "reload";                     # Reload Environment
            "${modifier}+q" = "kill";                       # Kill
            "${modifier}+f" = "fullscreen toggle";          # Fullscreen
            "${modifier}+h" = "floating toggle";            # Floating

            "${modifier}+Left" = "focus left";              # Focus
            "${modifier}+Right" = "focus right";
            "${modifier}+Up" = "focus up";
            "${modifier}+Down" = "focus down";

            "${modifier}+Shift+Left" = "move left";         # Move
            "${modifier}+Shift+Right" = "move right";
            "${modifier}+Shift+Up" = "move up";
            "${modifier}+Shift+Down" = "move down";

            "Alt+Left" = "workspace prev_on_output";        # Navigate workspaces
            "Alt+Right" = "workspace next_on_output";

            "Alt+1" = "workspace number 1";                 # Open Workspace
            "Alt+2" = "workspace number 2";
            "Alt+3" = "workspace number 3";

            "Alt+Shift+Left" = "move container to workspace prev, workspace prev";    # Move to Workspace
            "Alt+Shift+Right" = "move container to workspace next, workspace next";

            "Alt+Shift+1" = "move container to workspace number 1";     # Move to Workspace
            "Alt+Shift+2" = "move container to workspace number 2";
            "Alt+Shift+3" = "move container to workspace number 3";
            "Alt+Shift+4" = "move container to workspace number 4";
            "Alt+Shift+5" = "move container to workspace number 5";

            "Control+Up" = "resize shrink height 20px";     # Resize
            "Control+Down" = "resize grow height 20px";
            "Control+Left" = "resize shrink width 20px";
            "Control+Right" = "resize grow width 20px";

            "Print" = "exec ${pkgs.flameshot}/bin/flameshot gui"; # Screenshots

            "XF86AudioLowerVolume" = "exec ${pkgs.pamixer}/bin/pamixer -d 10";   # Volume
            "XF86AudioRaiseVolume" = "exec ${pkgs.pamixer}/bin/pamixer -i 10";
            "XF86AudioMute" = "exec ${pkgs.pamixer}/bin/pamixer -t";             # Media
            "XF86AudioMicMute" = "exec ${pkgs.pamixer}/bin/pamixer --default-source -t";

            "XF86MonBrightnessDown" = "exec ${pkgs.light}/bin/light -U  5";      # Brightness
            "XF86MonBrightnessUp" = "exec ${pkgs.light}/bin/light -A 5";
          };
        };
        extraConfig = ''
          set $opacity 0.8
          for_window [class=".*"] opacity 0.95
          for_window [app_id=".*"] opacity 0.95
          #for_window [app_id="thunar"] opacity 0.95, floating enable
          #for_window [app_id="Alacritty"] opacity $opacity
          #for_window [title="drun"] opacity $opacity
          #for_window [class="Emacs"] opacity $opacity
          #for_window [app_id="pavucontrol"] floating enable, sticky
          #for_window [app_id=".blueman-manager-wrapped"] floating enable
          for_window [title="Picture in picture"] floating enable, move position 1205 634, resize set 700 400, sticky enable

          # Window background blur
          blur on
          blur_xray off
          blur_passes 2
          blur_radius 5

          shadows on
          shadows_on_csd off
          shadow_blur_radius 20
          shadow_color #0000007F

          # inactive window fade amount. 0.0 = no dimming, 1.0 = fully dimmed
          default_dim_inactive 0.1
          dim_inactive_colors.unfocused #000000FF
          dim_inactive_colors.urgent #900000FF

          # Move minimized windows into Scratchpad (enable|disable)
          scratchpad_minimize disable
        '';                                    # $ swaymsg -t get_tree or get_outputs
        extraSessionCommands = ''
          #export WLR_NO_HARDWARE_CURSORS="1";  # Needed for cursor in vm
          export XDG_SESSION_TYPE=wayland
          export XDG_SESSION_DESKTOP=sway
          export XDG_CURRENT_DESKTOP=sway
        '';
      };
    };
  };
}
*/
