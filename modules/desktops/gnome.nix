#
# Configuration for GNOME Desktop. 
# Currently uses X11.
#

# Base Arguments
{ config 
, lib
, pkgs
# Added Variable Arguments
, username
, ...
}:

let
  inherit (lib) hm mkDefault mkEnableOption mkIf mkOption recursiveUpdate types;

  gnome-cfg = config.desktops.gnome;
  gdm-cfg = config.displayManager.gdm;

  # TODO: Complete
  gnome-default-keybindings = {
    focus-active-notification = [ "<super>n" ];
    open-application-menu = [ "<super>" ];

    screenshot = [ "<shift>Print" ];
    screenshot-window = [ "<alt>Print" ];
    show-screenshot-ui = [ "Print" ];

    switch-to-workspace-left = [ "<primary><super>left" "<primary><super>h" ];
    switch-to-workspace-right = [ "<primary><super>right" "<primary><super>l" ];

    # Keybinding to toggle "Show Applications" view
    #toggle-application-view
    # Keybinding to toggle the visibility of the notification list
    #toggle-message-tray
    # Keybinding to open overview (shows virtual desktops)
    # toggle-overview
    };
in {

  # MAYBE: TODO: Add Wayland functionality
  options = {
    desktops.gnome = {
      enable = mkEnableOption "GNOME Desktop Environment";

      keybindings = mkOption {
        type = with types; nullOr attrsOf listOf str;
        default = null;
        example = lib.literalExpression ''
          {
            switch-to-workspace-left = [ "<super><ctrl>left" "<super><ctrl>h" ];
            switch-to-workspace-right = [ "<super><ctrl>right" "<super><ctrl>l" ];
            # Move window/application to different workspace
            move-to-workspace-left =  [ "<super><shift>left" "<super><shift>h" ];
            move-to-workspace-right = [ "<super><shift>right" "<super><shift>l" ];
            close = [ "<super>q" "<alt>f4" ];
            toggle-fullscreen = [ "<super>f11" ];
          }
        '';
        description = ''
          Adds keybindings to the dconf database for `org.gnome.desktop.wm.keybindings`.
          For more information on dconf, see the man page for [dconf(1)](https://linux.die.net/man/1/dconf).

          If {var}`keybindings` is `null`, then `org.gnome.desktop.wm.keybindings` will not
          be edited.
        '';
      };
    };

    displayManager.gdm = {
      enable = mkOption { 
        default = gnome-cfg.enable;
        defaultText = lib.literalExpression ''
          if desktop.gnome.enable then true else false
        '';
        description = ''
          Whether to enable the GNOME Display Manager
        '';
      };
    };
  };

    
  config = let 

    # TODO: Complete
    gnome-default-keybindings = {
      focus-active-notification = [ "<super>n" ];
      open-application-menu = [ "<super>" ];

      screenshot = [ "<shift>Print" ];
      screenshot-window = [ "<alt>Print" ];
      show-screenshot-ui = [ "Print" ];

      switch-to-workspace-left = [ "<primary><super>left" "<primary><super>h" ];
      switch-to-workspace-right = [ "<primary><super>right" "<primary><super>l" ];

      # Keybinding to toggle "Show Applications" view
      #toggle-application-view
      # Keybinding to toggle the visibility of the notification list
      #toggle-message-tray
      # Keybinding to open overview (shows virtual desktops)
      # toggle-overview
    };

    # Returns element if not null else an empty attribute set
    #
    # Parameters:
    #   - replacer: what should be returned if `element` is null
    #   - element: either an AttrSet or is null
    #
    replaceNullWith = replacer: element:
      if (element == null) then replacer else element;


    # If `value` is null, return an empty attribute set.
    # Otherwise, attrset is formed from name and value
    combineNameWithNullable = name: value:
      lib.optionalAttrs (value != null) { name = value; };


  in mkIf gnome-cfg.enable {

    # Enable GNOME
    services.xserver = {
      enable = true;  # Enables X11

      displayManager.gdm.enable = gdm-cfg.enable;  # Display/Login Manager
      desktopManager.gnome.enable = true;  # Desktop Environment
    };
    services.udev.packages = with pkgs.gnome; [
      gnome-settings-daemon
    ];
	  
    # Default system packages & extensions
    environment.systemPackages = with pkgs.gnome; [
      dconf-editor
      gnome-tweaks
      nautilus
    ] ++ (with pkgs.gnomeExtensions; [
      paperwm
    ]);

    # Exclude the following from default install
    services.gnome.core-utilities.enable = false;
    environment.gnome.excludePackages = (with pkgs; [
      gnome-tour  # GNOME Greeter & Tour
      nano        # Terminal Text Editor
      xterm       # Terminal Emulator
    ]);
    # NOTE: Uncomment if specific default packages need to be excluded
    /*
    environment.gnome.excludePackages = ( with pkgs; [
      gnome-photos
      gnome-tour
    ]) ++ ( with pkgs.gnome; [
      gnome-characters
      gnome-contacts
      cheese    # webcam tool
      gnome-contacts  #
      gnome-music
      gnome-terminal
      gedit     # text editor
      epiphany  # web browser
      geary     # email reader
      evince    # document viewer
      totem     # video player
      tali      # poker game
      iagno     # go game
      hitori    # sudoku game
      atomix    # puzzle game
    ]);
    */
        
        
        
    # ---===[ Home Manager Options ]===--- #

    home-manager.users."${username}" = {
            
      # TODO: Once more dconf settings are added, switch to
      # ```nix
      # dconf.settings = foldl recursiveUpdate { ... } [ # Global settings
      #   (combineNameWithNullable "org/gnome/desktop/wm/keybindings" keybindings)
      #   (...)
      # ];
      # ```
      #dconf.settings = mkIf (gnome-cfg.keybindings != null) {
      #  "org/gnome/desktop/wm/keybindings" = gnome-cfg.keybindings;
      #};

    };

  };

}
