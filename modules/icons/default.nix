#
# Manually defined icons
#
# Referenced Files
# ================
#
# - flake.nix 
#    └─ ./modules
#        ├─ ./browsers 
#        │   └─ firefox.nix 
#        └─ ./icons
#            ├─ ./logos
# ---------> └─ default.nix *
#

{
  # ---=== Logos ===--- #
  logos = {
    github = ./logos/github.svg;
    gitlab = ./logos/gitlab.svg;
    nix = ./logos/nix-snowflake.svg;
    startpage = ./logos/startpage.ico;
    youtube = ./logos/youtube_144x144.png;
  };
}

