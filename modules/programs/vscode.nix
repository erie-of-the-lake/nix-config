#
# Configurations for vscodium
#
# NOTE: Home Manager is needed
#
# TODO: 
# - Configure system to not rely on `editor.FontFamily`
# - 

# Base Arguments
{ pkgs
, inputs
# Added Variable Arguments
, system
, username
, ...
}:

let
  inherit system;
  vscode-extensions = inputs.nix-vscode-extensions.extensions.${system};
in {
  # Dependencies 
  environment.systemPackages = with pkgs; [
    nixd  # Syntax-checking language server for Nix 
  ];

  home-manager.users.${username} = {
    programs.vscode = {
      enable = true;
      enableUpdateCheck = false;  # Enables update notifications
      enableExtensionUpdateCheck = false;  # Enables extension update checks

      package = pkgs.vscodium;  # Replace vscode w/ vscodium


      # ---===[ Extensions ]===--- #
        
      # Documentation on nix-vscode-extensions flake here:
      #   - https://github.com/nix-community/nix-vscode-extensions
      extensions = with vscode-extensions.vscode-marketplace; [
        arrterian.nix-env-selector  # Allows for nix environments/shells to be loadedcode runner
        dracula-theme.theme-dracula  # Enables dracula theme
        formulahendry.code-runner  # Allows code to be run in text editor
        jacobdufault.fuzzy-search  # Enables fuzzy search
        jnoortheen.nix-ide  # Enables nix lang syntax highlighting + IDE
        mhutchie.git-graph  # Enables git graph for current repository
        ms-python.pylint    # Python linter
        ms-python.python    # Rich presence for python language
        rust-lang.rust-analyzer  # Provides support + linting for Rust lang
        vadimcn.vscode-lldb  # Debugger for C++, Rust, and other compiled languages
        vscodevim.vim  # Provides vim-like keybindings
        yzhang.markdown-all-in-one  # Shortcuts + live preview for Markdown
    ];


      # ---===[ Global Snippets ]===--- #
      globalSnippets = {
        fixme = {
          body = [ "$LINE_COMMENT FIXME: $0 " ];
          description = "Insert a FIXME remark";
          prefix = [ "fixme" ];
        };
        hack = {
          body = [ "$LINE_COMMENT HACK: $0 " ];
          description = "Insert a HACK remark to indicate a bodge/hacky workaround";
          prefix = [ "hack" ];
        };
        note = {
          body = [ "$LINE_COMMENT NOTE: $0 " ];
          description = "Insert a NOTE remark";
          prefix = [ "note" ];
        };
      };


      # ---===[ User Settings ]===--- #
      userSettings = {
        # HACK: FIXME: enable way to not rely on this setting
        #"editor.fontFamily" = "'Comic Code', monospace";
        "editor.fontSize" = "13.5";  # Change font size - can be str or int
                                     #  - e.g. "13.5" or 14
        # Set color theme manually - won't work via extension
        "workbench.colorTheme" = "Dracula";  # Set color scheme
        # Edit nix tabsize
        "[nix]"."editor.tabSize" = 2;

        # Enable `nixd` Nix language server
        "nix.enableLanguageServer" = true;
        "nix.serverPath" = "nixd";

        "nix.serverSettings" = {
          /*
          "nil" = {
            #"diagnostics" = {
            #  "ignored" = [ "unused_binding" "unused_with" ];
            #};
              "formatting" = {
                #"command" = ["nixpkgs-fmt"];
              };
              "maxMemoryMB" = 6144;
              "flake" = {
                # Auto-archiving behavior which may use network.
                #
                # - null: Ask every time.
                # - true: Automatically run `nix flake archive` when necessary.
                # - false: Do not archive. Only load inputs that are already on disk.
                # Type: null | boolean
                # Example: true
                "autoArchive" = true;
                # Whether to auto-eval flake inputs.
                # The evaluation result is used to improve completion, but may cost
                # lots of time and/or memory.
                #
                # Type: boolean
                # Example: true
                "autoEvalInputs" = true;
                # The input name of nixpkgs for NixOS options evaluation.
                #
                # The options hierarchy is used to improve completion, but may cost
                # lots of time and/or memory.
                # If this value is `null` or is not found in the workspace flake's
                # inputs, NixOS options are not evaluated.
                #
                # Type: null | string
                # Example: "nixos"
                "nixpkgsInputName" = "nixpkgs";
              };
          };
          */
                
          "nixd" = {
            "eval" = {
              # Example target for writing a package.
              #target = {
              #    #args = [ "--expr" "with import <nixpkgs> { }; callPackage ./somePackage.nix { }" ];
              #    installable = '''';
              #};
              # Depth for evaluation
              depth = 4;
              # Number of workers for evaluation task
              workers = 4;
            };
            formatting.command = "nixpkgs-fmt";
            "options" = {
              "enable" = true;
              "target" = {
                "args" = [ ];
                # Example installable for flake-parts, nixos, and home-manager

                # flake-parts
                #installable = "/flakeref#debug.options";

                # nixOS configuration
                #installable = "/flakeref#nixosConfigurations.<adrastea>.options";

                # home-manager configuration
                #"installable" = ''/home/fia/.nix#homeConfigurations."rhim".options'';
              };
            };
          };
        };
      };
    };
  };
}
