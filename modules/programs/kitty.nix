#
# Configurations for kitty terminal
#
# NOTE: May need NixGL repository for kitty to run properly.

{ pkgs
, lib
, home-manager
, username
, ...
}:

{
  
  # TODO: Add to `environment.variables` as well
  environment.variables = {
    TERMINAL = lib.mkDefault "${pkgs.kitty}/bin/kitty";
  };

  home-manager.users.${username} = {
    programs.kitty = {
      enable = true;

      # See: "https://github.com/kovidgoyal/kitty-themes"
      theme = "Catppuccin-Mocha";

      settings = {
        background_opacity = "0.95";  # Float
        enable_audio_bell = false;
        ### Layouts
        # - Fat: 1 full vertical window on left w/ vertically separated windows
        #     on the right
        # - Tall: 
        enabled_layouts = "splits:split_axis=horizontal";
      };

      keybindings = let
        hsplitCommand = "launch --location=hsplit";
        vsplitCommand = "launch --location=vsplit";
      in {
        "ctrl+q"       = "close_window";
        "ctrl+shift+q" = "close_tab";
        "crtl+shift+w" = "";  # disable

        ### Window creation
        "ctrl+shift+enter" = "";  # disable new window
        # Vim keybindings
        "ctrl+shift+enter>h" = vsplitCommand;
        "ctrl+shift+enter>j" = hsplitCommand;
        "ctrl+shift+enter>k" = hsplitCommand;
        "ctrl+shift+enter>l" = vsplitCommand;
        # Arrow key keybindings
        "ctrl+shift+enter>up"    = hsplitCommand;
        "ctrl+shift+enter>down"  = hsplitCommand;
        "ctrl+shift+enter>left"  = vsplitCommand;
        "ctrl+shift+enter>right" = vsplitCommand;

        ### Window management
        "ctrl+shift+a" = "start_resizing_window";  # [A]djust windows
        "ctrl+shift+r" = "layout_action rotate";   # [R]otate window
      };
    };
  };

}
