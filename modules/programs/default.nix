#
# Configuration for programs classified under `programs` in Nix settings
#
# Referenced Files
# ================
#
# - flake.nix 
#    ├─ ./hosts
#    │   └─ default.nix <--- Module called here
#    └─ ./modules
#        └─ ./programs
# ---------> ├─ default.nix *
#            └─ <program>.nix 
#

[
    ./kitty.nix
    ./vscode.nix
]