#
# Nix flake devShell for RMarkdown documents
# Can be run with `$ nix develop` or `$ nix develop </path/to/flake.nix>#<host>`
#

{
  description = "";

  inputs = {
    # Nixpkgs Unstable
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    # Flake Utils
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = inputs @ { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = {
          inherit system;
          config.allowUnfree = true; # Uncomment if necessary
        };
        
        rMarkdownPackages = with pkgs.rPackages; [
          # Base Dependencies
          dplyr      # tool for working with dataframe-like objects
          glue       # implements interpreted string literals, like python f-strings
          here       # allows for relative filepaths
          knitr      # general purpose tool for dynamic report generation
          margrittr  # provides %>% operator
          readr      # for reading rectangular text data (e.g. csv, tsv, fwf)
          rmarkdown
          tinytex  # tiny LaTeX distribution based on TeX Live
          xfun     # library containing several utility functions

          # Additional Dependencies -- ADD HERE
          
        ];
      in {
        devShells.default = pkgs.mkShell {
          # Dependencies that need to be linked against
          buildInputs = with pkgs; [
            (rWrapper.override { packages = rMarkdownPackages; })
            (rstudioWrapper.override { packages = rMarkdownPackages; })
          ];

          # Dependencies that need to run at build-time or for shell hooks
          #nativeBuildInputs = [];
        };
      }
    );


}
