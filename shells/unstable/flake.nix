#
# Nix flake devShell
# Can be run with `$ nix develop` or `$ nix develop </path/to/flake.nix>#<host>`
#

{
  description = "";

  inputs = {
    # Nixpkgs Unstable
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    # Flake Utils
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = inputs @ { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = {
          inherit system;
          #config.allowUnfree = true; # Uncomment if necessary
        };
      in {
        devShells.default = pkgs.mkShell {
          # Dependencies that need to be linked against
          buildInputs = [];

          # Dependencies that need to run at build-time or for shell hooks
          nativeBuildInputs = [];
        };
      }
    );


}
