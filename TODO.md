# TODO

## Primary TODO

This every bullet within this list is of highest priority for eventual configuration

- [X] Update mentions of [*nixos.wiki*](https://nixos.wiki) in `modules/browsers/firefox.nix` to [*wiki.nixos.org*](https://wiki.nixos.org/wiki/NixOS_Wiki)
- [ ] Implement daemon to decrypt file containing Comic Code font via SOPS-NIX
- [ ] Configure AMD CPU options in `modules/hardware`, using the `nixos-hardware` flake if applicable- [ ] Configure `taruca` specialisations
  - [ ] Default configuration
  - [ ] VFIO configuration (passing through RX 6800XT)
- [ ] Make `fileSystems` module, incorporating options for the following types
  - [ ] btrfs
  - [ ] zfs
  - [ ] tmpfs (for impermanence on some partitions/subvolumes)
- [ ] Refactor parts of or finish TODOs in codebase
  - [ ] Have options defined `modules/` start with `local.<optionName>`
  - [ ] `hosts/`
    - [ ] `default.nix`
      - [ ] Make variables in `let` clause conform to Nix style guide
      - [ ] Remove need for `import`s (e.g `defaultDesktop = (import ../modules/browsers ++ import ../modules/editors)`)
    - [ ] `configuration.nix`
      - [ ] Configure an up-to-date kernel
      - [ ] Get `users.users.root.openssh.authorizedKeys.keyFiles` working
- [ ] Make overlays
  - [ ] Steam overlay
    - [ ] Delete mentions of Steam in `hosts/rhim/default.nix` after overlay is complete
  - [ ] Discord overlay


## Secondary TODO

Every item in this list is of eventual priority but is overshadowed by more prominent TODOs. I still may try to do these as a break from other TODOs.

- [ ] SwayFX configuration
  - [ ] Create a set of options
    - [ ] Create options for primary Sway configuration
    - [ ] Create options for SwayFX configuration (i.e. eye candy)
  - [ ] Implement said options onto machines with Desktop Environments
- [ ] Nixify `auto-cpufreq`
- [ ] Configure the following options in `modules/hardware`, using the `nixos-hardware` flake if applicable
  - [ ] Nvidia GPU options
    - [ ] dGPU only
    - [ ] Non-Nvidia iGPU only
    - [ ] PRIME Offloading
    - [ ] PRIME Sync
  - [ ] AMD GPU options



## Tertiary TODO

These TODOs highlight eventual mini-projects of interest, but for now, they aren't totally necessary.

- [ ] Configure VFIO for Nvidia dGPU

