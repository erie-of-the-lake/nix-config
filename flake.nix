# 
# Starting flake file of nix configurations.
# This flake and its associated directories follow closely from:
#   - https://github.com/MatthiasBenaets/nixos-config
#   - https://github.com/Misterio77/nix-config/tree/main
#
# by: Erin Shadrock
#
# 
# Referenced Files
# ================
#
# - flake.nix *
#    └─ ./hosts
#        └─ default.nix
# 

# TODO:
# - Add SOPS-NIX & secrets folder
# - 

{
  description = "Nix & NixOS system configuration via flakes";
	
  inputs = {
    
    # ---===[ Base Flake Inputs ]===--- #

    # Nixpkgs Unstable
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    
    # Home Manager - Manual @ https://nix-community.github.io/home-manager/
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Flake-Utils - Documentation @ https://github.com/numtide/flake-utils
    flake-utils.url = "github:numtide/flake-utils";
    
    # Nix User Repository - To potentially consider
    # 	- On NixOS, requires `nur.nixosModules.nur` to be added to host modules
    #   - On non-NixOS systems, requires `nur.hmModules.nur` to be added 
    nur.url = "github:nix-community/NUR";


    # ---===[ Misc. Flake Inputs ]===--- #

    # Auto-cpufreq - CPU Frequency Governor
    auto-cpufreq = {
      url = "github:AdnanHodzic/auto-cpufreq";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # NixGL - Enable if problems exist w/ OpenGL
    nixgl = {
      url = "github:guibou/nixGL";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    
    # NixOS Hardware - Hardware Defaults for different systems
    nixos-hardware.url = "github:NixOS/nixos-hardware";

    # NixVim - Neovim config system for Nix
    nixvim = {
      url = "github:nix-community/nixvim";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Nix VS Code Extensions - More up-to-date vscode extensions
    nix-vscode-extensions.url = "github:nix-community/nix-vscode-extensions";

    # Sops-Nix - Declaratively provision secrets on NixOS
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

  };
	
	
	
  outputs = inputs @ { 
    self
    , nixpkgs
    , home-manager
    , flake-utils
    , nur
    , ...
  }:
    let
      username = "fia";
      # Predefined User Directories
      directories = rec {
        home = "/home/${username}";
        flake = "${home}/.nix";
        xdgConfigHome = "${home}/.config";
      };
    in {

      # NixOS Configurations
      nixosConfigurations = (
        import ./hosts {
          # Inherited from Nixpkgs
          inherit (nixpkgs) lib;
          # Inputs & Flake Derived into Output
          inherit inputs nixpkgs home-manager flake-utils nur;
          # Variables defined in `let` clause
          inherit username directories;
        }
      );
      

      # NOTE: Deprecated for now
      # Home Manager Configurations
      /*
      homeConfigurations = (
        import ./hosts {
          # Inherited from Nixpkgs
          inherit (nixpkgs) lib;
          # Inputs & Flakes Derived into Output
          inherit inputs nixpkgs home-manager flake-utils nur;
          # Variables defined in `let` clause
          inherit username directories;
        }
      );
      */

    };
}
