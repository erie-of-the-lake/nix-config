# Erin's Nix & NixOS Configuration



# Systems

|          | Main Laptop | Main Desktop | Server | NixOS VM | Nix VM   |
|:---------|:-----------:|:------------:|:------:|:--------:|:--------:|
| Hostname | rhim        | --           | pudu   | sive     | oisin    |
| Device Name | Eluktronics RP-15 | N/A (Custom) | Raspberry Pi 4 2 GB | QEMU/KVM VM | QEMU/KVM VM |

# Installation Guide

First, we need to partition the boot device. On most modern desktops and laptops with an NVME boot drive, this will show up as `/dev/nvme0`. On virtual machines, the boot drive will typically be `/dev/vda`. For the rest of this installation guide, the boot drive will be designated as `/dev/sda`.

On every single system I use without RAID, I aim to use BTRFS due to its ability to snapshot as well as compress data, using the `zstd` compression algorithm unless otherwise specified.

When formatting and partitioning a new UEFI system with plenty of space, we use the following commands:
```console
# parted /dev/sda -- mklabel gpt
# parted /dev/sda -- mkpart primary 1G -8G    # main partition
# parted /dev/sda -- mkpart ESP fat32 0% 1G   # boot partition
# parted /dev/sda -- set 2 esp on
# parted /dev/sda -- mkpart priamry -8G 100%  # recovery partition
```
`/dev/sda3` is our recovery partition. If you opt out of making a recovery partition, then omit line 4 and change line 2 to be `parted /dev/sda -- mkpart primary 1G 100%`.
Note also that we do not make a swap partition, as a swapfile will be created using the `swapDevices` option in `hosts/<hostname>/hardware-configuration.nix`.

Now, when creating new filesystems, we want to use LUKS encryption on the majority of our devices for security. The rest of this guide will assume that to be the case. However, first, we set up our boot and recovery partitions.
```console
# mkfs.fat -F 32 -n boot /dev/sda2
# mkfs.ext4 -L recovery /dev/sda3
```
Again, omit line 2 if your installation does not include a recovery partition.

When creating a LUKS-encrypted BTRFS partition compatible with Timeshift, we execute the following commands:
```console
# cryptsetup luksFormat /dev/sda1 --label crypted
# cryptsetup luksOpen /dev/disk/by-label/crypted cryptdata
# mkfs.btrfs -L nixos /dev/mapper/cryptdata
# mount /dev/disk/by-label/nixos /mnt
# btrfs subvolume create /mnt/@      # make Timeshift-compatible subvolumes
# btrfs subvolume create /mnt/@home
# umount /mnt  # afterwards we mount our newly generated subvolumes
# mount -o rw,noatime,compress=zstd,space_cache=v2,subvol=@ /dev/disk/by-label/nixos /mnt
# mkdir -p /mnt/home
# mount -o rw,noatime,compress=zstd,space_cache=v2,subvol=@home /dev/disk/by-label/nixos /mnt/home
```

Finally, we mount the partition `boot` to `/mnt/boot` via
```console
# mkdir -p /mnt/boot
# mount /dev/disk/by-label/boot /mnt/boot
```

